package com.example.githubsearch.ui.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubsearch.data.network.RestApi
import com.example.githubsearch.data.network.dto.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val restApi: RestApi
): ViewModel() {

    val state = MutableLiveData<ViewState>(ViewState.Empty)

    private var loadingJob: Job? = null

    private var query = ""
    private var page = 1
    private val size = 20
    private var total = 0
    private val items = mutableListOf<Repository>()

    fun onQueryChanged(query: String) {
        this.query = query
        loadFirstPage()
    }

    fun onLoadNextPage() {
        loadNextPage()
    }

    private fun loadFirstPage() {
        loadingJob?.cancel()
        page = 1
        items.clear()
        loadingJob = viewModelScope.launch { search(query, page, size, true)}
    }

    private fun loadNextPage() {
        if (page * size < total) {
            loadingJob?.cancel()
            page += 1
            loadingJob = viewModelScope.launch { search(query, page, size, true)}
        }
    }

    suspend fun search(query: String, page: Int, size: Int, showLoading: Boolean) {
        if (query.isNotEmpty()) {
            try {
                if (showLoading) {
                    state.postValue(ViewState.Loading)
                }
                val repositories = restApi.searchRepositories(query, page, size)
                val items = repositories.items ?: emptyList()
                val total = repositories.totalCount ?: items.size
                if (this.total < total) {
                    this.total = total
                }
                this.items.addAll(items)
                state.postValue(ViewState.Data(query, this.total, this.items))
            } catch (error: Exception) {
                Log.e(SearchViewModel::class.simpleName, "", error)
                state.postValue(ViewState.Error(error))
            }
        } else {
            state.postValue(ViewState.Empty)
        }
    }
}

open class ViewState {
    object Empty : ViewState()
    object Loading : ViewState()
    data class Error(
        val error: Throwable
    ) : ViewState()
    data class Data(
        val query: String,
        val total: Int,
        val items: List<Repository>
    ) : ViewState()
}