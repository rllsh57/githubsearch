package com.example.githubsearch.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.githubsearch.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment() {

    private val model: SearchViewModel by viewModels()
    private lateinit var binding: FragmentSearchBinding
    private lateinit var adapter: RepositoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = RepositoryAdapter(model::onLoadNextPage)
        binding.list.adapter = adapter

        binding.editQuery.addTextChangedListener {
            model.onQueryChanged(it.toString())
        }

        model.state.observe(viewLifecycleOwner) {
            when (it) {
                is ViewState.Empty -> showLayout(binding.layoutEmpty.root)
                is ViewState.Loading -> showLayout(binding.layoutProgress.root)
                is ViewState.Error -> showError(it)
                is ViewState.Data -> showData(it)
            }
        }
    }

    private fun showLayout(layout: View) {
        binding.layoutContainer.children.forEach { it.visibility = View.GONE }
        layout.visibility = View.VISIBLE
    }

    private fun showData(state: ViewState.Data) {
        binding.textDataQuery.text = "total = ${state.total}, query = ${state.query}"
        adapter.updateItems(state.items)
        showLayout(binding.layoutData)
    }

    private fun showError(state: ViewState.Error) {
        binding.layoutError.error.text = state.error.stackTraceToString()
        showLayout(binding.layoutError.root)
    }
}