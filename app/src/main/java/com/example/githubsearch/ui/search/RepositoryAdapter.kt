package com.example.githubsearch.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.githubsearch.data.network.dto.Repository
import com.example.githubsearch.databinding.ItemRepositoryBinding

class RepositoryAdapter(
    val onNextPage: () -> Unit
): RecyclerView.Adapter<BindingViewHolder<ViewBinding>>() {

    private val items = mutableListOf<Repository>()

    override fun onBindViewHolder(holder: BindingViewHolder<ViewBinding>, position: Int) {
        val binding = holder.binding as ItemRepositoryBinding
        val item = items[position]
        binding.number.text = (position + 1).toString()
        binding.name.text = item.name ?: "-"

        if (position == items.size - 1) {
            onNextPage()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<ViewBinding> {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRepositoryBinding.inflate(inflater)
        return BindingViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateItems(newItems: List<Repository>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }
}

class BindingViewHolder<B : ViewBinding>(val binding: B) : RecyclerView.ViewHolder(binding.root)