package com.example.githubsearch.data.network

import com.example.githubsearch.data.network.dto.Repositories
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {

    @GET("/search/repositories")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") size: Int
    ): Repositories
}