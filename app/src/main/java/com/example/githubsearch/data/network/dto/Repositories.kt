package com.example.githubsearch.data.network.dto

import com.google.gson.annotations.SerializedName

data class Repositories(
    @SerializedName("incomplete_results") val incompleteResults: Boolean?,
    @SerializedName("items") val items: List<Repository>?,
    @SerializedName("total_count") val totalCount: Int?
)